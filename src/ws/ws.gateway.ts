import {
  OnGatewayConnection, OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway, WebSocketServer,
} from '@nestjs/websockets';
import * as WebSocket from 'ws';
import {Injectable} from '@nestjs/common';
import {Observable, Subject} from 'rxjs';
import {filter, map} from 'rxjs/operators';

type Client = WebSocket;

export interface IWsMessage<T = any> {
  event: string;
  data: any;
}

@Injectable()
@WebSocketGateway(3005, {namespace: 'events', transports: ['websocket']})
export class WsGateway implements OnGatewayConnection, OnGatewayDisconnect<Client> {

  @WebSocketServer() server;

  private clients: {[index: string]: Client} = {};

  private messageSubject$ = new Subject<IWsMessage>();

  getObservableForEvent<T>(eventName: string): Observable<T> {
    return this.messageSubject$.pipe(
        filter(message => message.event === eventName),
        map(message => message.data),
    );
  }

  handleConnection(client: Client, message) {
    const socketId = this.getSocketId(client);
    this.clients[socketId] = client;
    client.send(JSON.stringify({event: 'init', data: socketId}));
  }

  handleDisconnect(client: Client) {
    const socketId = this.getSocketId(client);
    delete this.clients[socketId];
  }

  @SubscribeMessage('message')
  handleMessage(client: any, payload: any) {
    // return 'Hello world 2!';
  }

  @SubscribeMessage('block')
  handleBlock(client: any, payload: any) {
    this.messageSubject$.next({event: 'block', data: {
      id: this.getSocketId(client),
      payload,
    }});
  }

  broadcast(eventName: string, data: any, excludeIds = []) {
    for (const idx in this.clients) {
      if (excludeIds.indexOf(idx) === -1) {
        this.clients[idx].send(JSON.stringify({
          event: eventName,
          data,
        }));
      }
    }
  }

  private getSocketId(client: Client) {
    const symbols = Object.getOwnPropertySymbols(client._socket);
    return client._socket[symbols[0]].toString();
  }

}
