import { Injectable } from '@nestjs/common';
import {WsGateway} from './ws/ws.gateway';

@Injectable()
export class AppService {

  constructor(
      private gateway: WsGateway,
  ) {
    const a = 5;
  }

  getHello(): string {
    return 'Hello World!';
  }
}
