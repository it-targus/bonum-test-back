import { Injectable } from '@nestjs/common';
import {IWsMessage, WsGateway} from '../ws/ws.gateway';
import {Observable} from 'rxjs';

export interface ISheduledTask {
    startTime: number;
    finishTime: number;
}
interface IBlockedSignal {
    selectedIntervals: Array<ISheduledTask>;
}
interface IBlockedCollection {
    [index: string]: Array<ISheduledTask>;
}

@Injectable()
export class ShedulerService {

    private block$: Observable<IBlockedSignal>;

    private blocked: IBlockedCollection = {};

    constructor(
        private gateway: WsGateway,
    ) {
        this.block$ = this.gateway.getObservableForEvent<IBlockedSignal>('block');
        this.block$.subscribe((data: any) => {
            this.blocked[data.id] = data.payload.selectedIntervals;
            this.gateway.broadcast('block', this.blocked, [data.id]);
        });
    }
}
