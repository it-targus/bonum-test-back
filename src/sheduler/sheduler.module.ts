import {HttpModule, Module} from '@nestjs/common';
import {ShedulerService} from './sheduler.service';
import {WsModule} from '../ws/ws.module';

@Module({
    imports: [
        HttpModule,
        WsModule,
    ],
    providers: [
        ShedulerService,
    ],
    exports: [
        ShedulerService,
    ],
})
export class ShedulerModule {
}
