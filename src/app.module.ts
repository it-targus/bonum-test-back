import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { WsModule } from './ws/ws.module';
import { ShedulerModule } from './sheduler/sheduler.module';

@Module({
  imports: [
    WsModule,
    ShedulerModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
