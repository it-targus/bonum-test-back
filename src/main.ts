import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {IoAdapter, WsAdapter} from '@nestjs/websockets';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.useWebSocketAdapter(new WsAdapter(app));
  // app.useWebSocketAdapter(new IoAdapter(app.getHttpServer()));
  await app.listen(3000);
}
bootstrap();
