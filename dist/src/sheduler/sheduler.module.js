"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const sheduler_service_1 = require("./sheduler.service");
const ws_module_1 = require("../ws/ws.module");
let ShedulerModule = class ShedulerModule {
};
ShedulerModule = __decorate([
    common_1.Module({
        imports: [
            common_1.HttpModule,
            ws_module_1.WsModule,
        ],
        providers: [
            sheduler_service_1.ShedulerService,
        ],
        exports: [
            sheduler_service_1.ShedulerService,
        ],
    })
], ShedulerModule);
exports.ShedulerModule = ShedulerModule;
//# sourceMappingURL=sheduler.module.js.map