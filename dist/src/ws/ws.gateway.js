"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const websockets_1 = require("@nestjs/websockets");
const common_1 = require("@nestjs/common");
const rxjs_1 = require("rxjs");
const operators_1 = require("rxjs/operators");
let WsGateway = class WsGateway {
    constructor() {
        this.clients = {};
        this.messageSubject$ = new rxjs_1.Subject();
    }
    afterInit() { }
    getObservableForEvent(eventName) {
        return this.messageSubject$.pipe(operators_1.filter(message => message.event === eventName), operators_1.map(message => message.data));
    }
    handleConnection(client, message) {
        const socketId = this.getSocketId(client);
        this.clients[socketId] = client;
        client.send(JSON.stringify({ event: 'init', data: socketId }));
    }
    handleDisconnect(client) {
        const socketId = this.getSocketId(client);
        delete this.clients[socketId];
    }
    handleMessage(client, payload) {
    }
    handleBlock(client, payload) {
        this.messageSubject$.next({ event: 'block', data: {
                id: this.getSocketId(client),
                payload,
            } });
    }
    broadcast(eventName, data, excludeIds = []) {
        for (const idx in this.clients) {
            if (excludeIds.indexOf(idx) === -1) {
                this.clients[idx].send(JSON.stringify({
                    event: eventName,
                    data,
                }));
            }
        }
    }
    getSocketId(client) {
        const symbols = Object.getOwnPropertySymbols(client._socket);
        return client._socket[symbols[0]].toString();
    }
};
__decorate([
    websockets_1.WebSocketServer(),
    __metadata("design:type", Object)
], WsGateway.prototype, "server", void 0);
__decorate([
    websockets_1.SubscribeMessage('message'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], WsGateway.prototype, "handleMessage", null);
__decorate([
    websockets_1.SubscribeMessage('block'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object, Object]),
    __metadata("design:returntype", void 0)
], WsGateway.prototype, "handleBlock", null);
WsGateway = __decorate([
    common_1.Injectable(),
    websockets_1.WebSocketGateway(3005, { namespace: 'events', transports: ['websocket'] })
], WsGateway);
exports.WsGateway = WsGateway;
//# sourceMappingURL=ws.gateway.js.map