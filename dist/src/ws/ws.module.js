"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const ws_gateway_1 = require("./ws.gateway");
let WsModule = class WsModule {
};
WsModule = __decorate([
    common_1.Module({
        providers: [
            ws_gateway_1.WsGateway,
        ],
        exports: [
            ws_gateway_1.WsGateway,
        ],
    })
], WsModule);
exports.WsModule = WsModule;
//# sourceMappingURL=ws.module.js.map